import { ColumnType } from 'typeorm';
import { IndexOptions } from 'typeorm/decorator/options/IndexOptions';
import { ValidationOptions } from 'class-validator/types/decorator/ValidationOptions';

export interface PublValidateCommonOptions {
  /**
   * IsInt, IsBoolean, IsString, IsEnum, IsOptional
   */
  ValidationOptions?: ValidationOptions;
  /**
   * default: true.
   */
  isOptional?: boolean;
  /**
   * Default: true.
   */
  isColumn?: boolean;
  /**
   * ColumnType
   */
  columnType?: ColumnType;
  /**
   * for snake.
   */
  columnName?: string;
  /**
   * Default: false.
   */
  isIndex?: boolean;
  /**
   * Column size.
   */
  length?: number;
  /**
   * Default value.
   */
  default?: any;
  /**
   * Default: false.
   */
  nullable?: boolean;
  /**
   * IsIndex options
   */
  indexOptions?: IndexOptions;
}

export interface PublValidateNumberOptions extends PublValidateCommonOptions {
  /**
   * default: false.
   */
  // isPK?: boolean;
  /**
   * Default: all. @InInt()
   */
  coverage?: NumberCoverage | NumberCoverage[];
}

export const NumberCoverage = {
  /**
   * Greater than zero.
   */
  POSITIVE: 'POSITIVE',
  /**
   * Smaller than zero.
   */
  NEGATIVE: 'NEGATIVE',
  /**
   * Greater than or equal zero.
   */
  MIN_ZERO: 'MIN_ZERO',
  /**
   * Greater than or equal zero.
   */
  MAX_ZERO: 'MAX_ZERO',
} as const;
export type NumberCoverage = typeof NumberCoverage[keyof typeof NumberCoverage];

export interface PublValidateEnumOptions extends PublValidateCommonOptions {
  /**
   * Enum vs Key value object. Default: false
   */
  isConst?: boolean;
  /**
   * Enum as const
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  enum: object;
}
