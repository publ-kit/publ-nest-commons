import { isObject } from '@nestjs/common/utils/shared.utils';
import { ClassConstructor } from 'class-transformer/types/interfaces';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { flat } from './publ-utils';
/**
 * parseJSON + objectToDTO
 * @param cls: Return type
 * @param data
 * @param errorMessages
 * @param namingConvertOptions
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export async function stringToDTO<T extends object>(
  cls: ClassConstructor<T>,
  data: string,
  errorMessages?: string[],
  namingConvertOptions: NamingConvertOptions = NamingConvertOptions.NONE,
): Promise<T> {
  return objectToDTO(
    cls,
    parseJSON(data, errorMessages),
    errorMessages,
    namingConvertOptions,
  );
}
/**
 * String to JSON Object.
 *
 * try-catch 포함 자질구레한 예외처리 과정 모음.
 * @param str: JSON object 로 파싱할 원문.
 * @param message: 비지니스 로직 상 들어갈 에러메시지.
 */
export const parseJSON = (
  str: string,
  message?: string[],
  // eslint-disable-next-line @typescript-eslint/ban-types
): object => {
  if (!str.length) {
    throw new Error(sumErrorMessageArray('Empty JSON string.', message));
  }
  try {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const result = JSON.parse(str);
    // TODO: length 의미가 있는지?
    if (!Object.keys(result).length) {
      throw new Error(sumErrorMessageArray('Empty JSON string.', message));
    }
    /**
     * HAPPY CASE
     */
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return result;
  } catch (e) {
    if (e instanceof Error) {
      if (message?.length) {
        e.message = message + '\n' + e.message
      }
      throw e
    }

    throw new Error(sumErrorMessageArray(JSON.stringify(e), message));
  }
};
/**
 * plainToClass(cls, keysToCamel(data))
 * @param cls Class
 * @param data instance object
 * @param errorMessages?
 * @param namingConvertOptions
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export async function objectToDTO<T extends object>(
  cls: ClassConstructor<T>,
  data: object,
  errorMessages?: string[],
  namingConvertOptions: NamingConvertOptions = NamingConvertOptions.NONE,
): Promise<T> {
  const result = plainToClass(
    cls,
    namingConvertOptions === NamingConvertOptions.CAMEL
      ? keysSwapCamelAndSnake(data)
      : namingConvertOptions === NamingConvertOptions.SNAKE
      ? keysSwapCamelAndSnake(data, true)
      : data
  );
  const errors = await validate(result);
  if (errors.length > 0) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
    const message: string[] = flat(errors.map((e) => Object.values(e.constraints)));
    // TODO: Generic http exception type
    throw new Error(sumErrorMessageArray(message, errorMessages));
  }
  return result;
}
/**
 * Ref: https://matthiashager.com/converting-snake-case-to-camel-case-object-keys-with-javascript
 */
export const keysSwapCamelAndSnake = (obj: any, isToSnake = false): any => {
  if (isObject(obj)) {
    if (!Object.values(obj).length) {
      return obj;
    }
    const n = {};
    Object.keys(obj).forEach(
      (k) =>
        (n[isToSnake ? toSnake(k) : toCamel(k)] = keysSwapCamelAndSnake(
          obj[k],
          isToSnake,
        )),
    );
    return n;
  } else if (Array.isArray(obj)) {
    return obj.map((i) => keysSwapCamelAndSnake(i));
  }
  return obj;
};

export const toCamel = (snakeString: string): string =>
  snakeString.replace(/([-_][a-z])/gi, ($1) =>
    $1.toUpperCase().replace('-', '').replace('_', ''),
  );

export const toSnake = (camelString: string): string =>
  camelString
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase();

const sumErrorMessageArray = (messages: string | string[], prefix?: string[]): string =>
  typeof messages === 'string'
    ? prefix?.length ? prefix.join('\n') + '\n' + messages : messages
    : (prefix?.length ? prefix.concat(messages) : messages).join('\n')

export const NamingConvertOptions = {
  CAMEL: 'CAMEL',
  SNAKE: 'SNAKE',
  NONE: 'NONE',
} as const;
export declare type NamingConvertOptions =
  typeof NamingConvertOptions[keyof typeof NamingConvertOptions];

