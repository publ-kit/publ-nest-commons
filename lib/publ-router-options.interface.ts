import { RequestMethod, Type } from '@nestjs/common';

export interface PublRouterOptions {
  /**
   * 컨트롤러 이후의 API 엔드포인트.
   */
  path?: string;
  /**
   * HTTP 메서드.
   */
  method: RequestMethod;
  /**
   * 스웨거 API 타이틀란.
   */
  summary?: string;
  /**
   * 스웨거 API 드롭다운 내부에 위치한 상세설명란.
   */
  description?: string;
  /**
   * HTTP Header 에 X-Publ-Chanel-Id 항목을 사용할지 여부. Default: true.
   */
  useXPublChannelIdHeader?: boolean;
  /**
   * POST, PUT, PATCH 시 샤용하는 Body 의 타입(DTO).
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  bodyType?: string | Function;
  /**
   * Default: keyValue
   */
  responseStructure?: ResponseStructure
  /**
   * Publ 전용 리스폰스 중 data 의 키값.
   */
  responseName: string;
  /**
   * 리스폰스의 타입(DTO).
   */
  // eslint-disable-next-line @typescript-eslint/ban-types
  responseType: string | Function;
  /**
   * 리스폰스 타입에 대한 부가설명.
   *
   * Default: '하단 스키마 목록 중 ${responseType.name} 부분 참고.'
   */
  responseDescription?: string;
  /**
   * 에러 리스폰스의 타입. Default: PublError.
   */
  errorType?: Type<unknown>;
  /**
   * 에러 리스폰스 타입에 대한 부가설명.
   *
   * Default: '400: bad request 에러를 대표로 에러 타입의 정의를 표출함. 하단 스키마 목록 중 ${ errorResponseType.name ?? PublError.name } 부분 참고.'
   */
  errorTypeDescription?: string;
}

export const ResponseStructure = {
  keyValue: 'keyValue',
  array: 'array',
  itSelf: 'itSelf',
} as const;
export type ResponseStructure =
  typeof ResponseStructure[keyof typeof ResponseStructure];
