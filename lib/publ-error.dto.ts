import { ApiProperty } from '@nestjs/swagger';
/**
 * Ref: https://publ.atlassian.net/wiki/spaces/PUBL/pages/3997735/Publ+API
 */
export class PublError {
  @ApiProperty({ example: '[ABILITY_NAME]-00000', type: 'string' })
  code: string;

  @ApiProperty({ type: 'string' })
  msg: string;

  @ApiProperty({ description: `Optional`, type: 'any' })
  data?: any;

  constructor(prefix: string, data?: any) {
    /**
     * data instanceOf PublErrorInterface
     */
    if (data?.msg && data?.code) {
      this.code = prefix + '-' + data?.code;
      this.msg = data?.msg;
      this.data = data?.data;
      return;
    }
    /**
     * JWT Error
     */
    if (data?.name && data?.message) {
      this.code = prefix;
      this.msg = data?.message;
      /**
       * TODO: JWT error code number injectable.
       */
      switch (data?.name) {
        case 'TokenExpiredError':
          this.data = data?.expiredAt
            ? { [data?.name]: data?.expiredAt }
            : data?.name;
          this.code += '-0002';
          break;
        case 'JsonWebTokenError':
          this.data = data?.inner ? { [data?.name]: data?.inner } : data?.name;
          this.code += '-0003';
          break;
        case 'NotBeforeError':
          this.data = data?.date ? { [data?.name]: data?.date } : data?.name;
          this.code += '-0004';
          break;
        case 'Error': // data?.message === 'No auth token'
          this.code += '-0001';
          break;
      }
      return;
    }
    /**
     * else
     */
    this.code = prefix + '-' + (data?.code || 'ERROR-CODE-NOT-DEFINED');
    this.msg = data?.message || data?.msg || 'INTERNAL_SERVER_ERROR';
  }
}
