import {
  PublValidateCommonOptions,
  PublValidateEnumOptions,
  PublValidateNumberOptions,
} from './publ-columns.interface';
import { IsBoolean, IsDate, IsEnum, IsInt, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { applyDecorators } from '@nestjs/common';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { commons, numberCoverage } from './publ-columns.helper';
import { Column, CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';
import { ToBoolean } from './publ-to-boolean.decorator';

export function PublNumber(
  options?: PublValidateNumberOptions
  // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    Type(() => Number),
    IsInt(options?.ValidationOptions),
    ...(options?.coverage
      ? Array.isArray(options?.coverage)
        ? options.coverage.map((coverage) => numberCoverage(coverage))
        : [numberCoverage(options.coverage)]
      : []),
    // ...(options?.isPK
    //   ? [
    //     PrimaryGeneratedColumn({
    //       /**
    //        * WithPrecisionColumnType 사용하는 일반 컬럼일 가능성 때문에,
    //        * PK 일 경우 PrimaryGeneratedColumnType 으로 묵시적 다운캐스팅 처리함.
    //        */
    //       type: (options?.columnType as PrimaryGeneratedColumnType) ?? undefined,
    //       name: options?.columnName,
    //     }),
    //   ]
    //   : commons(options)
    // )
    ...commons(options)
  );
}

export function PublBoolean(
  options?: PublValidateCommonOptions,
  // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    /**
     * Ref: https://github.com/typestack/class-transformer/issues/306
     */
    // Transform(({ value }) => value === ('true' || true)),
    ToBoolean(),
    IsBoolean(options?.ValidationOptions),
    ...commons(options),
  );
}

export function PublString(
  options?: PublValidateCommonOptions
  // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    Type(() => String),
    IsString(options?.ValidationOptions),
    ...(options?.length ? [MaxLength(options?.length)]: []),
    ...(commons(options)),
  );
}

export function PublEnum(
  options?: PublValidateEnumOptions,
  // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  const enumObject = options?.isConst ? Object.values(options?.enum) : options?.enum
  return applyDecorators(
    ApiProperty({ enum: enumObject, default: options?.default }),
    IsEnum(enumObject, options?.ValidationOptions),
    ...commons(options),
  );
}
/**
 *
 * @param name optional
 * @param isOptional default: true
 * @param needConvert default: true
 * @constructor
 */
export function PublDate(
  name: string,
  isOptional = true,
  needConvert = true
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    IsDate(),
    Column({ name, nullable: isOptional , type: 'timestamptz'}),
    ...(isOptional ? [IsOptional()] : []),
    ...(needConvert ? [Type(() => Date)] : [])
  );
}
/**
 *
 * @param name default: created_at
 * @param isOptional default: true
 * @constructor
 */
export function PublCreateDate(
  name = 'created_at',
  isOptional = true,
  // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    IsDate(),
    CreateDateColumn({ name, nullable: isOptional, type: 'timestamptz' }),
    ...(isOptional ? [IsOptional()] : []),
  );
}
/**
 *
 * @param name default: updated_at
 * @param isOptional default: true
 * @constructor
 */
export function PublUpdateDate(
  name = 'updated_at',
  isOptional = true, // eslint-disable-next-line @typescript-eslint/ban-types
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    IsDate(),
    UpdateDateColumn({ name, nullable: isOptional, type: 'timestamptz' }),
    ...(isOptional ? [IsOptional()] : []),
  );
}
/**
 *
 * @param name default: deleted_at
 * @param isOptional default: true
 * @constructor
 */
export function PublDeleteDate(
  name = 'deleted_at',
  isOptional = true,
): <TFunction extends Function, Y>(
  // eslint-disable-next-line @typescript-eslint/ban-types
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(
    IsDate(),
    DeleteDateColumn({ name, nullable: isOptional, type: 'timestamptz' }),
    ...(isOptional ? [IsOptional()] : []),
  );
}
