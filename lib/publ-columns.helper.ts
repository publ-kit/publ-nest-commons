import { IsNegative, IsOptional, IsPositive, Max, Min } from 'class-validator';
import { PublValidateCommonOptions, NumberCoverage } from './publ-columns.interface';
import { Column, Index } from 'typeorm';

export const commons = (
  options?: PublValidateCommonOptions,
): PropertyDecorator[] => [
  /**
   * TODO: check default, required in swagger cli, typeorm
   */
  ...(options?.isIndex ? [Index(options?.indexOptions)] : []),
  ...(typeof options?.isOptional === 'undefined' || options?.isOptional === true ? [IsOptional()] : []),
  ...(typeof options?.isColumn === 'undefined' || options?.isColumn === true
    ? [
      // TODO: ColumnOptions
      Column({
        nullable: options?.nullable ?? (typeof options?.isOptional === 'undefined' || options?.isOptional),
        type: options?.columnType,
        name: options?.columnName,
        length: options?.length,
        default: options?.default,
        enum: options?.['enum'],
      }),
    ]
    : []),
];

export const numberCoverage = (type: NumberCoverage): PropertyDecorator => {
  switch (type) {
    case NumberCoverage.POSITIVE: return IsPositive;
    case NumberCoverage.NEGATIVE: return IsNegative;
    case NumberCoverage.MIN_ZERO: return Min(0);
    case NumberCoverage.MAX_ZERO: return Max(0);
  }
};
