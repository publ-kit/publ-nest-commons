import * as Joi from 'joi';
/**
 * ConfigModule.forRoot.validationSchema 에서 사용
 * @param options
 * @param requiredNumbers
 */
export const validationSchema = (
  options: Record<string, unknown>,
  requiredNumbers: string[],
): Joi.ObjectSchema<object> =>
  Joi.object(
    Object.keys(options).reduce((accumulator, currentValue) => {
      accumulator[currentValue] = requiredNumbers.includes(currentValue)
        ? Joi.number().required()
        : Joi.string().required();
      return accumulator;
    }, {}),
  );
/**
 * typeof data === Array<String>
 * Ref: https://stackoverflow.com/questions/23130292/test-for-array-of-string-type-in-typescript
 */
export const isArrayOfString = (fn: any): fn is string[] =>
  Array.isArray(fn) && !!fn.length && fn.every((e) => typeof e === 'string');
/**
 * Ref: https://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays
 */
export const flat = (e) => (Array.isArray(e) ? [].concat.apply([], e.map(flat)) : e);
// /**
//  * https://stackoverflow.com/a/38340730/13170735
//  * @param obj
//  */
// // eslint-disable-next-line @typescript-eslint/ban-types
// export const removeEmpty = (obj: object): object =>
//   Object.entries(obj)
//     .filter(([_, v]) => v != null)
//     .reduce(
//       (acc, [k, v]) => ({ ...acc, [k]: v === Object(v) ? removeEmpty(v) : v }),
//       {},
//     );
/**
 * TODO: FIX
 *
 * https://stackoverflow.com/a/38340730/13170735
 * @param obj
 * @param isRemoved
 */
export const convertEmpty = <T extends object>(obj: T, isRemoved = false): T =>
  (isRemoved
      ? // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-unsafe-return
      Object.entries(obj).filter(([_, v]) => v != null)
      : // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-unsafe-return
      Object.entries(obj).map(([_, v]) => (v === null ? undefined : v))
  ).reduce(
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-assignment,spellcheck/spell-checker
    (accumulator, [k, v]) => ({ ...accumulator, [k]: v === Object(v) ? convertEmpty(v) : v }),
    {},
  ) as T;

// TODO: convertEmpty array

// TODO: Validate exist?
export const arrayToString = (str: string): string[] => str.split(/\r?\n/);
