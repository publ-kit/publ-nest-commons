import {
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Inject,
  Logger,
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { PublError } from './publ-error.dto';
/**
 * HTTP status code 에 따른 NestJS exception class 들을 사용하기 때문에,
 * HTTPException Custom class 새로 구현하지 않고,
 * 필터 단계에서만 publ error type 에 맞게 수정함.
 *
 * Ref: https://github.com/nestjs/nest/blob/master/packages/core/exceptions/base-exception-filter.ts
 */
export class PublHTTPExceptionsFilter<T = any> extends BaseExceptionFilter {
  readonly logger = new Logger('ExceptionsHandler');
  abilityName: string;

  constructor(
    @Inject('ERROR_CODE_PREFIX') private readonly ERROR_CODE_PREFIX: string,
  ) {
    super();
    this.abilityName = ERROR_CODE_PREFIX;
  }

  catch(exception: T, host: ArgumentsHost) {
    const applicationRef =
      this.applicationRef ||
      (this.httpAdapterHost && this.httpAdapterHost.httpAdapter);
    /**
     * exception.getStatus() === exception.response.statusCode
     *
     * exception.response === {
     *    statusCode: number,
     *    message: object',
     *    error: string
     * }
     */
    const data =
      exception instanceof HttpException ? exception.getResponse() : undefined;

    const code =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    /**
     * TODO
     * https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/jsonwebtoken/index.d.ts
     * https://github.com/typeorm/typeorm/tree/master/src/error
     * https://github.com/nestjs/nest/blob/master/packages/common/enums/http-status.enum.ts
     * https://stackoverflow.com/questions/58993405/how-can-i-handle-typeorm-error-in-nestjs
     */

    applicationRef?.reply(
      host.getArgByIndex(1),
      new PublError(this.abilityName, data),
      code,
    );
  }
}
