import { PublRouterOptions, ResponseStructure } from './publ-router-options.interface';
import { applyDecorators, Header, HttpCode, HttpStatus, RequestMethod } from '@nestjs/common';
import { ReferenceObject, SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ApiBadRequestResponse, ApiBody, ApiHeader, ApiOkResponse, ApiOperation, getSchemaPath } from '@nestjs/swagger';
import { RequestMapping } from '@nestjs/common/decorators/http/request-mapping.decorator';
import { PublError } from './publ-error.dto';

export function PublRouter(options: PublRouterOptions) {
  const isCreateUpdate =
    options.method ===
    (RequestMethod.POST || RequestMethod.PUT || RequestMethod.PATCH);
  /**
   * Ref: https://swagger.io/specification/#data-types
   * Ref: https://docs.nestjs.com/openapi/types-and-parameters#extra-models
   */
  const response: SchemaObject | ReferenceObject =
    typeof options.responseType === 'string'
      ? { type: options.responseType }
      : { $ref: getSchemaPath(options.responseType) };

  return applyDecorators(
    RequestMapping({ path: options.path, method: options.method }),
    Header('Accept', 'application/json'),
    Header('Content-Type', 'application/json'),
    // TODO: Fix
    /**
     * 퍼블 공통 리스폰스 타입에 기반한 예시와 간단 설명부.
     */
    ApiOkResponse({
      schema:
        options?.responseStructure === ResponseStructure.itSelf
          ? response
          : {
            properties: {
              data: {
                properties: {
                  [options.responseName]:
                    options?.responseStructure === ResponseStructure.array
                      ? { items: response }
                      : response,
                },
              },
            },
          },
      description:
        options?.responseDescription ??
        `하단 스키마 목록 중 ${
          typeof options?.responseType === 'string'
            ? options?.responseType
            : options?.responseType.name
        } 부분 참고.`,
    }),
    /**
     * 400: bad request 에러를 대표로 에러 타입의 정의를 표출. Default: PublError
     */
    ApiBadRequestResponse({
      type: options?.errorType || PublError,
      description:
        '400: bad request 에러를 대표로 에러 타입의 정의를 표출함. ' +
        options?.errorTypeDescription ??
        `하단 스키마 목록 중 ${
          options?.errorType?.name ?? PublError.name
        } 부분 참조`,
    }),
    /**
     *
     */
    ...(typeof options.useXPublChannelIdHeader === 'undefined' || options.useXPublChannelIdHeader
      ? [
        ApiHeader({
          name: 'X-Publ-Channel-Id',
          schema: { default: '1' },
          description: 'HTTP HEADER',
        }),
      ]
      : []),
    /**
     * Optional: 스웨거 API 타이틀 및 드롭다운 내부에 위치한 상세설명부.
     */
    ...(options.summary || options.description
      ? [
        ApiOperation({
          summary: options.summary,
          description: options.description,
        }),
      ]
      : []),
    /**
     * Optional: POST, PUT, PATCH 에 필요한 Body 타입에 대한 참조를 추가.
     */
    ...(isCreateUpdate && options.bodyType
      ? [ApiBody({ schema: { $ref: getSchemaPath(options.bodyType) } })]
      : []),
    /**
     * Optional: POST, PUT, PATCH 에서도 status code 200 으로 통일.
     */
    ...(isCreateUpdate ? [HttpCode(HttpStatus.OK)] : []),
  );
}
