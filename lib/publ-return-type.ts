/**
 * Ref: https://github.com/microsoft/TypeScript/wiki/Performance#preferring-interfaces-over-intersections
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export interface PublReturn<T extends object> { data: { [key: string]: T } }
// eslint-disable-next-line @typescript-eslint/ban-types
export interface PublReturns<T extends object> {
  // https://stackoverflow.com/questions/45258216/property-is-not-assignable-to-string-index-in-interface
  data: {
    [key: string]: T[],
    // pagination: Pagination
  },
}

export interface Pagination {
  totalEntries: number;
  limit: number;
  currentPage: number;
}
