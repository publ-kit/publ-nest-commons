# README #

### Install ###

`npm i ssh://bitbucket.org/publ-kit/publ-nest-commons.git#vX.X.X`

최신 버전 수는 커밋의 태그를 참조.  
버전 명시 없이 설치 시 자동으로 최신 커밋 시점을 기준으로 설치되나, 관리차원에서 명시적으로 버전을 입력하는 것을 권장함.

### Decorator parameter ###

- 자세한 내용은 `PublRouterOptions` 참고.
- `responseKey`: 리스폰스 데이터의 키로 적용. `{ data: { [responseKey]: ... }`
- `postBodyExample`: 인스턴스 형태의 오브젝트를 넣으면 예제 부분에 `JSON.stringify` 적용되나,   
  DTO 등 커스텀 타입을 직접 넣을 땐 적용되지 않고 빈 객체로 적용됨.  
  DTO 스트링 타입 캐스팅 예시: DTO에 기본값이 적용된 `constructor`를 추가한 후,  
  데코레이터에 `postBodyExample: JSON.stringify(new MyCustomDTO())` 형태로 입력해야 함.  
- `isArrayResponse`: Default `false`.
- `errorResponseType`: Default `PublErrorDto`.
