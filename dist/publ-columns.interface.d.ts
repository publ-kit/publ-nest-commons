import { ColumnType } from 'typeorm';
import { IndexOptions } from 'typeorm/decorator/options/IndexOptions';
import { ValidationOptions } from 'class-validator/types/decorator/ValidationOptions';
export interface PublValidateCommonOptions {
    /**
     * IsInt, IsBoolean, IsString, IsEnum, IsOptional
     */
    ValidationOptions?: ValidationOptions;
    /**
     * default: true.
     */
    isOptional?: boolean;
    /**
     * Default: true.
     */
    isColumn?: boolean;
    /**
     * ColumnType
     */
    columnType?: ColumnType;
    /**
     * for snake.
     */
    columnName?: string;
    /**
     * Default: false.
     */
    isIndex?: boolean;
    /**
     * Column size.
     */
    length?: number;
    /**
     * Default value.
     */
    default?: any;
    /**
     * Default: false.
     */
    nullable?: boolean;
    /**
     * IsIndex options
     */
    indexOptions?: IndexOptions;
}
export interface PublValidateNumberOptions extends PublValidateCommonOptions {
    /**
     * default: false.
     */
    /**
     * Default: all. @InInt()
     */
    coverage?: NumberCoverage | NumberCoverage[];
}
export declare const NumberCoverage: {
    /**
     * Greater than zero.
     */
    readonly POSITIVE: "POSITIVE";
    /**
     * Smaller than zero.
     */
    readonly NEGATIVE: "NEGATIVE";
    /**
     * Greater than or equal zero.
     */
    readonly MIN_ZERO: "MIN_ZERO";
    /**
     * Greater than or equal zero.
     */
    readonly MAX_ZERO: "MAX_ZERO";
};
export declare type NumberCoverage = typeof NumberCoverage[keyof typeof NumberCoverage];
export interface PublValidateEnumOptions extends PublValidateCommonOptions {
    /**
     * Enum vs Key value object. Default: false
     */
    isConst?: boolean;
    /**
     * Enum as const
     */
    enum: object;
}
