"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublError = void 0;
const swagger_1 = require("@nestjs/swagger");
/**
 * Ref: https://publ.atlassian.net/wiki/spaces/PUBL/pages/3997735/Publ+API
 */
class PublError {
    constructor(prefix, data) {
        /**
         * data instanceOf PublErrorInterface
         */
        if ((data === null || data === void 0 ? void 0 : data.msg) && (data === null || data === void 0 ? void 0 : data.code)) {
            this.code = prefix + '-' + (data === null || data === void 0 ? void 0 : data.code);
            this.msg = data === null || data === void 0 ? void 0 : data.msg;
            this.data = data === null || data === void 0 ? void 0 : data.data;
            return;
        }
        /**
         * JWT Error
         */
        if ((data === null || data === void 0 ? void 0 : data.name) && (data === null || data === void 0 ? void 0 : data.message)) {
            this.code = prefix;
            this.msg = data === null || data === void 0 ? void 0 : data.message;
            /**
             * TODO: JWT error code number injectable.
             */
            switch (data === null || data === void 0 ? void 0 : data.name) {
                case 'TokenExpiredError':
                    this.data = (data === null || data === void 0 ? void 0 : data.expiredAt)
                        ? { [data === null || data === void 0 ? void 0 : data.name]: data === null || data === void 0 ? void 0 : data.expiredAt }
                        : data === null || data === void 0 ? void 0 : data.name;
                    this.code += '-0002';
                    break;
                case 'JsonWebTokenError':
                    this.data = (data === null || data === void 0 ? void 0 : data.inner) ? { [data === null || data === void 0 ? void 0 : data.name]: data === null || data === void 0 ? void 0 : data.inner } : data === null || data === void 0 ? void 0 : data.name;
                    this.code += '-0003';
                    break;
                case 'NotBeforeError':
                    this.data = (data === null || data === void 0 ? void 0 : data.date) ? { [data === null || data === void 0 ? void 0 : data.name]: data === null || data === void 0 ? void 0 : data.date } : data === null || data === void 0 ? void 0 : data.name;
                    this.code += '-0004';
                    break;
                case 'Error': // data?.message === 'No auth token'
                    this.code += '-0001';
                    break;
            }
            return;
        }
        /**
         * else
         */
        this.code = prefix + '-' + ((data === null || data === void 0 ? void 0 : data.code) || 'ERROR-CODE-NOT-DEFINED');
        this.msg = (data === null || data === void 0 ? void 0 : data.message) || (data === null || data === void 0 ? void 0 : data.msg) || 'INTERNAL_SERVER_ERROR';
    }
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: '[ABILITY_NAME]-00000', type: 'string' }),
    __metadata("design:type", String)
], PublError.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: 'string' }),
    __metadata("design:type", String)
], PublError.prototype, "msg", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ description: `Optional`, type: 'any' }),
    __metadata("design:type", Object)
], PublError.prototype, "data", void 0);
exports.PublError = PublError;
//# sourceMappingURL=publ-error.dto.js.map