/**
 * https://github.com/typestack/class-transformer/issues/550
 * https://github.com/typestack/class-transformer/issues/306
 * @constructor
 */
declare const ToBoolean: () => (target: any, key: string) => void;
export { ToBoolean };
