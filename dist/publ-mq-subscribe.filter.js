"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublMQSubscribeFilter = void 0;
const core_1 = require("@nestjs/core");
const common_1 = require("@nestjs/common");
const publ_error_dto_1 = require("./publ-error.dto");
let PublMQSubscribeFilter = class PublMQSubscribeFilter extends core_1.BaseExceptionFilter {
    constructor(ERROR_CODE_PREFIX) {
        super();
        this.ERROR_CODE_PREFIX = ERROR_CODE_PREFIX;
        this.logger = new common_1.Logger('ExceptionsHandler');
        this.abilityName = ERROR_CODE_PREFIX;
    }
    catch(exception, host) {
        const applicationRef = this.applicationRef ||
            (this.httpAdapterHost && this.httpAdapterHost.httpAdapter);
        /**
         * exception.getStatus() === exception.response.statusCode
         *
         * exception.response === {
         *    statusCode: number,
         *    message: object',
         *    error: string
         * }
         */
        const data = exception instanceof common_1.HttpException ? exception.getResponse() : undefined;
        const code = exception instanceof common_1.HttpException
            ? exception.getStatus()
            : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
        console.log(code, new publ_error_dto_1.PublError(this.abilityName, data));
    }
};
PublMQSubscribeFilter = __decorate([
    common_1.Catch(),
    __param(0, common_1.Inject('ERROR_CODE_PREFIX')),
    __metadata("design:paramtypes", [String])
], PublMQSubscribeFilter);
exports.PublMQSubscribeFilter = PublMQSubscribeFilter;
//# sourceMappingURL=publ-mq-subscribe.filter.js.map