"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publCommon = exports.PublCommonBody = exports.PublCommon = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const publ_error_dto_1 = require("./publ-error.dto");
const request_mapping_decorator_1 = require("@nestjs/common/decorators/http/request-mapping.decorator");
function PublCommon(method, path, responseKey, responseType, isArrayResponse, responseDescription, errorResponseType, errorResponseDescription) {
    return publCommon(method, path, responseKey, responseType, isArrayResponse, responseDescription, errorResponseType, errorResponseDescription);
}
exports.PublCommon = PublCommon;
function PublCommonBody(method, path, responseKey, responseType, postBodyExample, isArrayResponse, responseDescription, errorResponseType, errorResponseDescription) {
    return (0, common_1.applyDecorators)((0, common_1.HttpCode)(common_1.HttpStatus.OK), (0, swagger_1.ApiBody)({ schema: { $ref: (0, swagger_1.getSchemaPath)(postBodyExample) } }), publCommon(method, path, responseKey, responseType, isArrayResponse, responseDescription, errorResponseType, errorResponseDescription));
}
exports.PublCommonBody = PublCommonBody;
function publCommon(method, path, responseKey, responseType, isArrayResponse = false, 
/**
 * 거의 건드릴 일 없을 것
 */
responseDescription, errorResponseType = publ_error_dto_1.PublError, errorResponseDescription = '하단 스키마 목록 중 PublErrorDto 부분 참조') {
    /**
     * Ref: https://swagger.io/specification/#data-types
     * Ref: https://docs.nestjs.com/openapi/types-and-parameters#extra-models
     */
    const response = responseType === ('string' || 'integer' || 'number' || 'boolean')
        ? // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            { type: responseType }
        : { $ref: (0, swagger_1.getSchemaPath)(responseType) };
    return (0, common_1.applyDecorators)((0, request_mapping_decorator_1.RequestMapping)({ path, method }), (0, common_1.Header)('Accept', 'application/json'), (0, common_1.Header)('Content-Type', 'application/json'), (0, swagger_1.ApiOkResponse)({
        schema: {
            properties: {
                data: {
                    properties: {
                        [responseKey]: isArrayResponse ? { items: response } : response,
                    },
                },
            },
        },
        description: responseType !== ('string' || 'integer' || 'number' || 'boolean') &&
            !responseDescription
            ? `하단 스키마 목록 중 ${
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
            responseType.name || responseType} 부분 참고.`
            : responseDescription || undefined,
    }), (0, swagger_1.ApiBadRequestResponse)({
        type: errorResponseType,
        description: '400 bad request 에러를 대표로 에러 타입의 정의를 표출함. ' + errorResponseDescription,
    }));
}
exports.publCommon = publCommon;
//# sourceMappingURL=publ-common.decorator.js.map