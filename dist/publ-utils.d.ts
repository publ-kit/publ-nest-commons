import * as Joi from 'joi';
/**
 * ConfigModule.forRoot.validationSchema 에서 사용
 * @param options
 * @param requiredNumbers
 */
export declare const validationSchema: (options: Record<string, unknown>, requiredNumbers: string[]) => Joi.ObjectSchema<object>;
/**
 * typeof data === Array<String>
 * Ref: https://stackoverflow.com/questions/23130292/test-for-array-of-string-type-in-typescript
 */
export declare const isArrayOfString: (fn: any) => fn is string[];
/**
 * Ref: https://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays
 */
export declare const flat: (e: any) => any;
/**
 * TODO: FIX
 *
 * https://stackoverflow.com/a/38340730/13170735
 * @param obj
 * @param isRemoved
 */
export declare const convertEmpty: <T extends object>(obj: T, isRemoved?: boolean) => T;
export declare const arrayToString: (str: string) => string[];
