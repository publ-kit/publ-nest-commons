import { PublValidateCommonOptions, PublValidateEnumOptions, PublValidateNumberOptions } from './publ-columns.interface';
export declare function PublNumber(options?: PublValidateNumberOptions): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
export declare function PublBoolean(options?: PublValidateCommonOptions): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
export declare function PublString(options?: PublValidateCommonOptions): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
export declare function PublEnum(options?: PublValidateEnumOptions): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
/**
 *
 * @param name optional
 * @param isOptional default: true
 * @param needConvert default: true
 * @constructor
 */
export declare function PublDate(name: string, isOptional?: boolean, needConvert?: boolean): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
/**
 *
 * @param name default: created_at
 * @param isOptional default: true
 * @constructor
 */
export declare function PublCreateDate(name?: string, isOptional?: boolean): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
/**
 *
 * @param name default: updated_at
 * @param isOptional default: true
 * @constructor
 */
export declare function PublUpdateDate(name?: string, isOptional?: boolean): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
/**
 *
 * @param name default: deleted_at
 * @param isOptional default: true
 * @constructor
 */
export declare function PublDeleteDate(name?: string, isOptional?: boolean): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
