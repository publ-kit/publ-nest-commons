/**
 * Ref: https://publ.atlassian.net/wiki/spaces/PUBL/pages/3997735/Publ+API
 */
export declare class PublError {
    code: string;
    msg: string;
    data?: any;
    constructor(prefix: string, data?: any);
}
