/**
 * Ref: https://github.com/microsoft/TypeScript/wiki/Performance#preferring-interfaces-over-intersections
 */
export interface PublReturn<T extends object> {
    data: {
        [key: string]: T;
    };
}
export interface PublReturns<T extends object> {
    data: {
        [key: string]: T[];
    };
}
export interface Pagination {
    totalEntries: number;
    limit: number;
    currentPage: number;
}
