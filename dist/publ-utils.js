"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.arrayToString = exports.convertEmpty = exports.flat = exports.isArrayOfString = exports.validationSchema = void 0;
const Joi = require("joi");
/**
 * ConfigModule.forRoot.validationSchema 에서 사용
 * @param options
 * @param requiredNumbers
 */
const validationSchema = (options, requiredNumbers) => Joi.object(Object.keys(options).reduce((accumulator, currentValue) => {
    accumulator[currentValue] = requiredNumbers.includes(currentValue)
        ? Joi.number().required()
        : Joi.string().required();
    return accumulator;
}, {}));
exports.validationSchema = validationSchema;
/**
 * typeof data === Array<String>
 * Ref: https://stackoverflow.com/questions/23130292/test-for-array-of-string-type-in-typescript
 */
const isArrayOfString = (fn) => Array.isArray(fn) && !!fn.length && fn.every((e) => typeof e === 'string');
exports.isArrayOfString = isArrayOfString;
/**
 * Ref: https://stackoverflow.com/questions/10865025/merge-flatten-an-array-of-arrays
 */
const flat = (e) => (Array.isArray(e) ? [].concat.apply([], e.map(exports.flat)) : e);
exports.flat = flat;
// /**
//  * https://stackoverflow.com/a/38340730/13170735
//  * @param obj
//  */
// // eslint-disable-next-line @typescript-eslint/ban-types
// export const removeEmpty = (obj: object): object =>
//   Object.entries(obj)
//     .filter(([_, v]) => v != null)
//     .reduce(
//       (acc, [k, v]) => ({ ...acc, [k]: v === Object(v) ? removeEmpty(v) : v }),
//       {},
//     );
/**
 * TODO: FIX
 *
 * https://stackoverflow.com/a/38340730/13170735
 * @param obj
 * @param isRemoved
 */
const convertEmpty = (obj, isRemoved = false) => (isRemoved
    ? // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-unsafe-return
        Object.entries(obj).filter(([_, v]) => v != null)
    : // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-unsafe-return
        Object.entries(obj).map(([_, v]) => (v === null ? undefined : v))).reduce(
// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-assignment,spellcheck/spell-checker
(accumulator, [k, v]) => (Object.assign(Object.assign({}, accumulator), { [k]: v === Object(v) ? (0, exports.convertEmpty)(v) : v })), {});
exports.convertEmpty = convertEmpty;
// TODO: convertEmpty array
// TODO: Validate exist?
const arrayToString = (str) => str.split(/\r?\n/);
exports.arrayToString = arrayToString;
//# sourceMappingURL=publ-utils.js.map