"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NamingConvertOptions = exports.toSnake = exports.toCamel = exports.keysSwapCamelAndSnake = exports.objectToDTO = exports.parseJSON = exports.stringToDTO = void 0;
const shared_utils_1 = require("@nestjs/common/utils/shared.utils");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const publ_utils_1 = require("./publ-utils");
/**
 * parseJSON + objectToDTO
 * @param cls: Return type
 * @param data
 * @param errorMessages
 * @param namingConvertOptions
 */
// eslint-disable-next-line @typescript-eslint/ban-types
async function stringToDTO(cls, data, errorMessages, namingConvertOptions = exports.NamingConvertOptions.NONE) {
    return objectToDTO(cls, (0, exports.parseJSON)(data, errorMessages), errorMessages, namingConvertOptions);
}
exports.stringToDTO = stringToDTO;
/**
 * String to JSON Object.
 *
 * try-catch 포함 자질구레한 예외처리 과정 모음.
 * @param str: JSON object 로 파싱할 원문.
 * @param message: 비지니스 로직 상 들어갈 에러메시지.
 */
const parseJSON = (str, message) => {
    if (!str.length) {
        throw new Error(sumErrorMessageArray('Empty JSON string.', message));
    }
    try {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const result = JSON.parse(str);
        // TODO: length 의미가 있는지?
        if (!Object.keys(result).length) {
            throw new Error(sumErrorMessageArray('Empty JSON string.', message));
        }
        /**
         * HAPPY CASE
         */
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return result;
    }
    catch (e) {
        if (e instanceof Error) {
            if (message === null || message === void 0 ? void 0 : message.length) {
                e.message = message + '\n' + e.message;
            }
            throw e;
        }
        throw new Error(sumErrorMessageArray(JSON.stringify(e), message));
    }
};
exports.parseJSON = parseJSON;
/**
 * plainToClass(cls, keysToCamel(data))
 * @param cls Class
 * @param data instance object
 * @param errorMessages?
 * @param namingConvertOptions
 */
// eslint-disable-next-line @typescript-eslint/ban-types
async function objectToDTO(cls, data, errorMessages, namingConvertOptions = exports.NamingConvertOptions.NONE) {
    const result = (0, class_transformer_1.plainToClass)(cls, namingConvertOptions === exports.NamingConvertOptions.CAMEL
        ? (0, exports.keysSwapCamelAndSnake)(data)
        : namingConvertOptions === exports.NamingConvertOptions.SNAKE
            ? (0, exports.keysSwapCamelAndSnake)(data, true)
            : data);
    const errors = await (0, class_validator_1.validate)(result);
    if (errors.length > 0) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
        const message = (0, publ_utils_1.flat)(errors.map((e) => Object.values(e.constraints)));
        // TODO: Generic http exception type
        throw new Error(sumErrorMessageArray(message, errorMessages));
    }
    return result;
}
exports.objectToDTO = objectToDTO;
/**
 * Ref: https://matthiashager.com/converting-snake-case-to-camel-case-object-keys-with-javascript
 */
const keysSwapCamelAndSnake = (obj, isToSnake = false) => {
    if ((0, shared_utils_1.isObject)(obj)) {
        if (!Object.values(obj).length) {
            return obj;
        }
        const n = {};
        Object.keys(obj).forEach((k) => (n[isToSnake ? (0, exports.toSnake)(k) : (0, exports.toCamel)(k)] = (0, exports.keysSwapCamelAndSnake)(obj[k], isToSnake)));
        return n;
    }
    else if (Array.isArray(obj)) {
        return obj.map((i) => (0, exports.keysSwapCamelAndSnake)(i));
    }
    return obj;
};
exports.keysSwapCamelAndSnake = keysSwapCamelAndSnake;
const toCamel = (snakeString) => snakeString.replace(/([-_][a-z])/gi, ($1) => $1.toUpperCase().replace('-', '').replace('_', ''));
exports.toCamel = toCamel;
const toSnake = (camelString) => camelString
    .split(/(?=[A-Z])/)
    .join('_')
    .toLowerCase();
exports.toSnake = toSnake;
const sumErrorMessageArray = (messages, prefix) => typeof messages === 'string'
    ? (prefix === null || prefix === void 0 ? void 0 : prefix.length) ? prefix.join('\n') + '\n' + messages : messages
    : ((prefix === null || prefix === void 0 ? void 0 : prefix.length) ? prefix.concat(messages) : messages).join('\n');
exports.NamingConvertOptions = {
    CAMEL: 'CAMEL',
    SNAKE: 'SNAKE',
    NONE: 'NONE',
};
//# sourceMappingURL=publ-json-parser.js.map