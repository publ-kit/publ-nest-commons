import { ClassConstructor } from 'class-transformer/types/interfaces';
/**
 * parseJSON + objectToDTO
 * @param cls: Return type
 * @param data
 * @param errorMessages
 * @param namingConvertOptions
 */
export declare function stringToDTO<T extends object>(cls: ClassConstructor<T>, data: string, errorMessages?: string[], namingConvertOptions?: NamingConvertOptions): Promise<T>;
/**
 * String to JSON Object.
 *
 * try-catch 포함 자질구레한 예외처리 과정 모음.
 * @param str: JSON object 로 파싱할 원문.
 * @param message: 비지니스 로직 상 들어갈 에러메시지.
 */
export declare const parseJSON: (str: string, message?: string[]) => object;
/**
 * plainToClass(cls, keysToCamel(data))
 * @param cls Class
 * @param data instance object
 * @param errorMessages?
 * @param namingConvertOptions
 */
export declare function objectToDTO<T extends object>(cls: ClassConstructor<T>, data: object, errorMessages?: string[], namingConvertOptions?: NamingConvertOptions): Promise<T>;
/**
 * Ref: https://matthiashager.com/converting-snake-case-to-camel-case-object-keys-with-javascript
 */
export declare const keysSwapCamelAndSnake: (obj: any, isToSnake?: boolean) => any;
export declare const toCamel: (snakeString: string) => string;
export declare const toSnake: (camelString: string) => string;
export declare const NamingConvertOptions: {
    readonly CAMEL: "CAMEL";
    readonly SNAKE: "SNAKE";
    readonly NONE: "NONE";
};
export declare type NamingConvertOptions = typeof NamingConvertOptions[keyof typeof NamingConvertOptions];
