"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.numberCoverage = exports.commons = void 0;
const class_validator_1 = require("class-validator");
const publ_columns_interface_1 = require("./publ-columns.interface");
const typeorm_1 = require("typeorm");
const commons = (options) => {
    var _a;
    return [
        /**
         * TODO: check default, required in swagger cli, typeorm
         */
        ...((options === null || options === void 0 ? void 0 : options.isIndex) ? [(0, typeorm_1.Index)(options === null || options === void 0 ? void 0 : options.indexOptions)] : []),
        ...(typeof (options === null || options === void 0 ? void 0 : options.isOptional) === 'undefined' || (options === null || options === void 0 ? void 0 : options.isOptional) === true ? [(0, class_validator_1.IsOptional)()] : []),
        ...(typeof (options === null || options === void 0 ? void 0 : options.isColumn) === 'undefined' || (options === null || options === void 0 ? void 0 : options.isColumn) === true
            ? [
                // TODO: ColumnOptions
                (0, typeorm_1.Column)({
                    nullable: (_a = options === null || options === void 0 ? void 0 : options.nullable) !== null && _a !== void 0 ? _a : (typeof (options === null || options === void 0 ? void 0 : options.isOptional) === 'undefined' || (options === null || options === void 0 ? void 0 : options.isOptional)),
                    type: options === null || options === void 0 ? void 0 : options.columnType,
                    name: options === null || options === void 0 ? void 0 : options.columnName,
                    length: options === null || options === void 0 ? void 0 : options.length,
                    default: options === null || options === void 0 ? void 0 : options.default,
                    enum: options === null || options === void 0 ? void 0 : options['enum'],
                }),
            ]
            : []),
    ];
};
exports.commons = commons;
const numberCoverage = (type) => {
    switch (type) {
        case publ_columns_interface_1.NumberCoverage.POSITIVE: return class_validator_1.IsPositive;
        case publ_columns_interface_1.NumberCoverage.NEGATIVE: return class_validator_1.IsNegative;
        case publ_columns_interface_1.NumberCoverage.MIN_ZERO: return (0, class_validator_1.Min)(0);
        case publ_columns_interface_1.NumberCoverage.MAX_ZERO: return (0, class_validator_1.Max)(0);
    }
};
exports.numberCoverage = numberCoverage;
//# sourceMappingURL=publ-columns.helper.js.map