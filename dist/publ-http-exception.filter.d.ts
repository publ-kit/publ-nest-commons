import { ArgumentsHost, Logger } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
/**
 * HTTP status code 에 따른 NestJS exception class 들을 사용하기 때문에,
 * HTTPException Custom class 새로 구현하지 않고,
 * 필터 단계에서만 publ error type 에 맞게 수정함.
 *
 * Ref: https://github.com/nestjs/nest/blob/master/packages/core/exceptions/base-exception-filter.ts
 */
export declare class PublHTTPExceptionsFilter<T = any> extends BaseExceptionFilter {
    private readonly ERROR_CODE_PREFIX;
    readonly logger: Logger;
    abilityName: string;
    constructor(ERROR_CODE_PREFIX: string);
    catch(exception: T, host: ArgumentsHost): void;
}
