import { ClassConstructor } from 'class-transformer/types/interfaces';
import { BadRequestException } from '@nestjs/common';
/**
 * String to JSON Object.
 *
 * try-catch 포함 자질구레한 예외처리 과정 모음.
 * @param str: JSON object 로 파싱할 원문.
 * @param message: 비지니스 로직 상 들어갈 에러메시지.
 * @param isReturnError: 웹소켓용 옵션.
 */
export declare const parseJSON: (str: string, message?: string[], isReturnError?: boolean) => object;
export declare function stringToDTO<T extends object>(cls: ClassConstructor<T>, data: string, errorMessages?: string[], isReturnError?: boolean, isSnakeToCamel?: boolean): Promise<T | BadRequestException>;
/**
 * plainToClass(cls, keysToCamel(data))
 * @param cls Class
 * @param data instance object
 * @param errorMessages?
 * @param isReturnError
 * @param isSnakeToCamel
 */
export declare function objectToDTO<T extends object>(cls: ClassConstructor<T>, data: object, errorMessages?: string[], isReturnError?: boolean, isSnakeToCamel?: boolean): Promise<T | BadRequestException>;
