"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NumberCoverage = void 0;
exports.NumberCoverage = {
    /**
     * Greater than zero.
     */
    POSITIVE: 'POSITIVE',
    /**
     * Smaller than zero.
     */
    NEGATIVE: 'NEGATIVE',
    /**
     * Greater than or equal zero.
     */
    MIN_ZERO: 'MIN_ZERO',
    /**
     * Greater than or equal zero.
     */
    MAX_ZERO: 'MAX_ZERO',
};
//# sourceMappingURL=publ-columns.interface.js.map