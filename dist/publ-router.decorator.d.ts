import { PublRouterOptions } from './publ-router-options.interface';
export declare function PublRouter(options: PublRouterOptions): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
