import { PublValidateCommonOptions, NumberCoverage } from './publ-columns.interface';
export declare const commons: (options?: PublValidateCommonOptions) => PropertyDecorator[];
export declare const numberCoverage: (type: NumberCoverage) => PropertyDecorator;
