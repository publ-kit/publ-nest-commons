import { RequestMethod, Type } from '@nestjs/common';
export declare function PublCommon(method: RequestMethod, path: string, responseKey: string, responseType: any, isArrayResponse?: boolean, responseDescription?: string, errorResponseType?: Type<unknown>, errorResponseDescription?: string): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
export declare function PublCommonBody(method: RequestMethod.POST | RequestMethod.PUT | RequestMethod.PATCH, path: string, responseKey: string, responseType: any, postBodyExample: string | Function, isArrayResponse?: boolean, responseDescription?: string, errorResponseType?: Type<unknown>, errorResponseDescription?: string): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
export declare function publCommon(method: RequestMethod, path: string, responseKey: string, responseType: any, isArrayResponse?: boolean, 
/**
 * 거의 건드릴 일 없을 것
 */
responseDescription?: string, errorResponseType?: Type<unknown>, errorResponseDescription?: string): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
