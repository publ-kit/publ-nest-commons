"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.objectToDTO = exports.stringToDTO = exports.parseJSON = void 0;
const shared_utils_1 = require("@nestjs/common/utils/shared.utils");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const common_1 = require("@nestjs/common");
const publ_utils_1 = require("./publ-utils");
/**
 * String to JSON Object.
 *
 * try-catch 포함 자질구레한 예외처리 과정 모음.
 * @param str: JSON object 로 파싱할 원문.
 * @param message: 비지니스 로직 상 들어갈 에러메시지.
 * @param isReturnError: 웹소켓용 옵션.
 */
const parseJSON = (str, message, isReturnError = false) => {
    if (!str.length) {
        return throwOrReturnError(message.length ? message.concat('Empty JSON string.') : message, isReturnError);
    }
    try {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const result = JSON.parse(str);
        // TODO: length 의미가 있는지?
        if (!Object.keys(result).length) {
            return throwOrReturnError(message.length ? message.concat('Empty JSON string.') : message, isReturnError);
        }
        /**
         * HAPPY CASE
         */
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return result;
    }
    catch (e) {
        const errMessage = message.length ? message : [];
        e instanceof Error
            ? errMessage.push(e.name, e.message)
            : errMessage.push(JSON.stringify(e));
        return throwOrReturnError(errMessage, isReturnError);
    }
};
exports.parseJSON = parseJSON;
/**
 * 입력값 유효성 검사용이라 BadRequestException 사용.
 * Socket 에서 사용 시, throw HTTPException 할 수 없음. 따라서 리턴으로 받을 수 있는 옵션이 필요함.
 * @param message
 * @param isErrorReturn
 */
const throwOrReturnError = (message, isErrorReturn) => {
    const error = new common_1.BadRequestException(message);
    if (!isErrorReturn) {
        throw error;
    }
    return error;
};
// eslint-disable-next-line @typescript-eslint/ban-types
async function stringToDTO(cls, data, errorMessages, isReturnError = false, isSnakeToCamel = false) {
    const result = exports.parseJSON(data, errorMessages, isReturnError);
    if (result instanceof common_1.BadRequestException) {
        return result;
    }
    return objectToDTO(cls, result, errorMessages, isReturnError, isSnakeToCamel);
}
exports.stringToDTO = stringToDTO;
/**
 * plainToClass(cls, keysToCamel(data))
 * @param cls Class
 * @param data instance object
 * @param errorMessages?
 * @param isReturnError
 * @param isSnakeToCamel
 */
// eslint-disable-next-line @typescript-eslint/ban-types
async function objectToDTO(cls, data, errorMessages, isReturnError = false, isSnakeToCamel = false) {
    1;
    const result = class_transformer_1.plainToClass(cls, isSnakeToCamel ? keysToCamel(data) : data);
    const errors = await class_validator_1.validate(result);
    if (errors.length > 0) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
        const message = publ_utils_1.flat(errors.map((e) => Object.values(e.constraints)));
        return throwOrReturnError((errorMessages === null || errorMessages === void 0 ? void 0 : errorMessages.length) ? errorMessages.concat(message) : message, isReturnError);
    }
    return result;
}
exports.objectToDTO = objectToDTO;
/**
 * Ref: https://matthiashager.com/converting-snake-case-to-camel-case-object-keys-with-javascript
 */
const toCamel = (str) => str.replace(/([-_][a-z])/gi, ($1) => $1.toUpperCase().replace('-', '').replace('_', ''));
const keysToCamel = (obj) => {
    if (shared_utils_1.isObject(obj)) {
        if (!Object.values(obj).length) {
            return obj;
        }
        const n = {};
        Object.keys(obj).forEach((k) => (n[toCamel(k)] = keysToCamel(obj[k])));
        return n;
    }
    else if (Array.isArray(obj)) {
        return obj.map((i) => keysToCamel(i));
    }
    return obj;
};
//# sourceMappingURL=publ-parser.js.map