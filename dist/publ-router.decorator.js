"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublRouter = void 0;
const publ_router_options_interface_1 = require("./publ-router-options.interface");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const request_mapping_decorator_1 = require("@nestjs/common/decorators/http/request-mapping.decorator");
const publ_error_dto_1 = require("./publ-error.dto");
function PublRouter(options) {
    var _a, _b, _c, _d;
    const isCreateUpdate = options.method ===
        (common_1.RequestMethod.POST || common_1.RequestMethod.PUT || common_1.RequestMethod.PATCH);
    /**
     * Ref: https://swagger.io/specification/#data-types
     * Ref: https://docs.nestjs.com/openapi/types-and-parameters#extra-models
     */
    const response = typeof options.responseType === 'string'
        ? { type: options.responseType }
        : { $ref: (0, swagger_1.getSchemaPath)(options.responseType) };
    return (0, common_1.applyDecorators)((0, request_mapping_decorator_1.RequestMapping)({ path: options.path, method: options.method }), (0, common_1.Header)('Accept', 'application/json'), (0, common_1.Header)('Content-Type', 'application/json'), 
    // TODO: Fix
    /**
     * 퍼블 공통 리스폰스 타입에 기반한 예시와 간단 설명부.
     */
    (0, swagger_1.ApiOkResponse)({
        schema: (options === null || options === void 0 ? void 0 : options.responseStructure) === publ_router_options_interface_1.ResponseStructure.itSelf
            ? response
            : {
                properties: {
                    data: {
                        properties: {
                            [options.responseName]: (options === null || options === void 0 ? void 0 : options.responseStructure) === publ_router_options_interface_1.ResponseStructure.array
                                ? { items: response }
                                : response,
                        },
                    },
                },
            },
        description: (_a = options === null || options === void 0 ? void 0 : options.responseDescription) !== null && _a !== void 0 ? _a : `하단 스키마 목록 중 ${typeof (options === null || options === void 0 ? void 0 : options.responseType) === 'string'
            ? options === null || options === void 0 ? void 0 : options.responseType
            : options === null || options === void 0 ? void 0 : options.responseType.name} 부분 참고.`,
    }), 
    /**
     * 400: bad request 에러를 대표로 에러 타입의 정의를 표출. Default: PublError
     */
    (0, swagger_1.ApiBadRequestResponse)({
        type: (options === null || options === void 0 ? void 0 : options.errorType) || publ_error_dto_1.PublError,
        description: (_b = '400: bad request 에러를 대표로 에러 타입의 정의를 표출함. ' +
            (options === null || options === void 0 ? void 0 : options.errorTypeDescription)) !== null && _b !== void 0 ? _b : `하단 스키마 목록 중 ${(_d = (_c = options === null || options === void 0 ? void 0 : options.errorType) === null || _c === void 0 ? void 0 : _c.name) !== null && _d !== void 0 ? _d : publ_error_dto_1.PublError.name} 부분 참조`,
    }), 
    /**
     *
     */
    ...(typeof options.useXPublChannelIdHeader === 'undefined' || options.useXPublChannelIdHeader
        ? [
            (0, swagger_1.ApiHeader)({
                name: 'X-Publ-Channel-Id',
                schema: { default: '1' },
                description: 'HTTP HEADER',
            }),
        ]
        : []), 
    /**
     * Optional: 스웨거 API 타이틀 및 드롭다운 내부에 위치한 상세설명부.
     */
    ...(options.summary || options.description
        ? [
            (0, swagger_1.ApiOperation)({
                summary: options.summary,
                description: options.description,
            }),
        ]
        : []), 
    /**
     * Optional: POST, PUT, PATCH 에 필요한 Body 타입에 대한 참조를 추가.
     */
    ...(isCreateUpdate && options.bodyType
        ? [(0, swagger_1.ApiBody)({ schema: { $ref: (0, swagger_1.getSchemaPath)(options.bodyType) } })]
        : []), 
    /**
     * Optional: POST, PUT, PATCH 에서도 status code 200 으로 통일.
     */
    ...(isCreateUpdate ? [(0, common_1.HttpCode)(common_1.HttpStatus.OK)] : []));
}
exports.PublRouter = PublRouter;
//# sourceMappingURL=publ-router.decorator.js.map