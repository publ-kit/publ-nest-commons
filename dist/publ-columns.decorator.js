"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublDeleteDate = exports.PublUpdateDate = exports.PublCreateDate = exports.PublDate = exports.PublEnum = exports.PublString = exports.PublBoolean = exports.PublNumber = void 0;
const class_validator_1 = require("class-validator");
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
const publ_columns_helper_1 = require("./publ-columns.helper");
const typeorm_1 = require("typeorm");
const publ_to_boolean_decorator_1 = require("./publ-to-boolean.decorator");
function PublNumber(options
// eslint-disable-next-line @typescript-eslint/ban-types
) {
    return (0, common_1.applyDecorators)((0, class_transformer_1.Type)(() => Number), (0, class_validator_1.IsInt)(options === null || options === void 0 ? void 0 : options.ValidationOptions), ...((options === null || options === void 0 ? void 0 : options.coverage)
        ? Array.isArray(options === null || options === void 0 ? void 0 : options.coverage)
            ? options.coverage.map((coverage) => (0, publ_columns_helper_1.numberCoverage)(coverage))
            : [(0, publ_columns_helper_1.numberCoverage)(options.coverage)]
        : []), 
    // ...(options?.isPK
    //   ? [
    //     PrimaryGeneratedColumn({
    //       /**
    //        * WithPrecisionColumnType 사용하는 일반 컬럼일 가능성 때문에,
    //        * PK 일 경우 PrimaryGeneratedColumnType 으로 묵시적 다운캐스팅 처리함.
    //        */
    //       type: (options?.columnType as PrimaryGeneratedColumnType) ?? undefined,
    //       name: options?.columnName,
    //     }),
    //   ]
    //   : commons(options)
    // )
    ...(0, publ_columns_helper_1.commons)(options));
}
exports.PublNumber = PublNumber;
function PublBoolean(options) {
    return (0, common_1.applyDecorators)(
    /**
     * Ref: https://github.com/typestack/class-transformer/issues/306
     */
    // Transform(({ value }) => value === ('true' || true)),
    (0, publ_to_boolean_decorator_1.ToBoolean)(), (0, class_validator_1.IsBoolean)(options === null || options === void 0 ? void 0 : options.ValidationOptions), ...(0, publ_columns_helper_1.commons)(options));
}
exports.PublBoolean = PublBoolean;
function PublString(options
// eslint-disable-next-line @typescript-eslint/ban-types
) {
    return (0, common_1.applyDecorators)((0, class_transformer_1.Type)(() => String), (0, class_validator_1.IsString)(options === null || options === void 0 ? void 0 : options.ValidationOptions), ...((options === null || options === void 0 ? void 0 : options.length) ? [(0, class_validator_1.MaxLength)(options === null || options === void 0 ? void 0 : options.length)] : []), ...((0, publ_columns_helper_1.commons)(options)));
}
exports.PublString = PublString;
function PublEnum(options) {
    const enumObject = (options === null || options === void 0 ? void 0 : options.isConst) ? Object.values(options === null || options === void 0 ? void 0 : options.enum) : options === null || options === void 0 ? void 0 : options.enum;
    return (0, common_1.applyDecorators)((0, swagger_1.ApiProperty)({ enum: enumObject, default: options === null || options === void 0 ? void 0 : options.default }), (0, class_validator_1.IsEnum)(enumObject, options === null || options === void 0 ? void 0 : options.ValidationOptions), ...(0, publ_columns_helper_1.commons)(options));
}
exports.PublEnum = PublEnum;
/**
 *
 * @param name optional
 * @param isOptional default: true
 * @param needConvert default: true
 * @constructor
 */
function PublDate(name, isOptional = true, needConvert = true) {
    return (0, common_1.applyDecorators)((0, class_validator_1.IsDate)(), (0, typeorm_1.Column)({ name, nullable: isOptional, type: 'timestamptz' }), ...(isOptional ? [(0, class_validator_1.IsOptional)()] : []), ...(needConvert ? [(0, class_transformer_1.Type)(() => Date)] : []));
}
exports.PublDate = PublDate;
/**
 *
 * @param name default: created_at
 * @param isOptional default: true
 * @constructor
 */
function PublCreateDate(name = 'created_at', isOptional = true) {
    return (0, common_1.applyDecorators)((0, class_validator_1.IsDate)(), (0, typeorm_1.CreateDateColumn)({ name, nullable: isOptional, type: 'timestamptz' }), ...(isOptional ? [(0, class_validator_1.IsOptional)()] : []));
}
exports.PublCreateDate = PublCreateDate;
/**
 *
 * @param name default: updated_at
 * @param isOptional default: true
 * @constructor
 */
function PublUpdateDate(name = 'updated_at', isOptional = true) {
    return (0, common_1.applyDecorators)((0, class_validator_1.IsDate)(), (0, typeorm_1.UpdateDateColumn)({ name, nullable: isOptional, type: 'timestamptz' }), ...(isOptional ? [(0, class_validator_1.IsOptional)()] : []));
}
exports.PublUpdateDate = PublUpdateDate;
/**
 *
 * @param name default: deleted_at
 * @param isOptional default: true
 * @constructor
 */
function PublDeleteDate(name = 'deleted_at', isOptional = true) {
    return (0, common_1.applyDecorators)((0, class_validator_1.IsDate)(), (0, typeorm_1.DeleteDateColumn)({ name, nullable: isOptional, type: 'timestamptz' }), ...(isOptional ? [(0, class_validator_1.IsOptional)()] : []));
}
exports.PublDeleteDate = PublDeleteDate;
//# sourceMappingURL=publ-columns.decorator.js.map