import { BaseExceptionFilter } from "@nestjs/core";
import { ArgumentsHost, Logger } from "@nestjs/common";
export declare class PublMQSubscribeFilter<T = any> extends BaseExceptionFilter {
    private readonly ERROR_CODE_PREFIX;
    readonly logger: Logger;
    abilityName: string;
    constructor(ERROR_CODE_PREFIX: string);
    catch(exception: T, host: ArgumentsHost): void;
}
